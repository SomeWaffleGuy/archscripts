#/bin/sh
#ArchSetup, my personal post-install Arch Linux setup script for KDE
#Doesn't do everything automatically because I'm lazy and nobody should use this
#Assume there's a set-up NetworkManager, GRUB, yay
#In-Repo Package Installs
sudo pacman -S xorg-server apparmor pipewire pipewire-alsa pipewire-pulse pipewire-jack cups cups-pdf samba ghostscript gsfonts cups-filters foomatic-db-engine foomatic-db foomatic-db-ppds foomatic-db-nonfree foomatic-db-nonfree-ppds gutenprint foomatic-db-gutenprint-ppds android-tools system-config-printer print-manager nss-mdns plasma-meta xdg-desktop-portal akonadi-calendar-tools akonadi-import-wizard akonadiconsole ark audiocd-kio dolphin dolphin-plugins ffmpegthumbs gwenview k3b kalarm kamera kamoso kate kcalc kcron cronie kdeconnect kdegraphics-mobipocket kdegraphics-thumbnailers kdenetwork-filesharing kdenlive kdepim-addons kdialog keditbookmarks kfloppy kio-extras kio-gdrive kipi-plugins kmag kmail kmail-account-wizard kmousetool kolourpaint konsole kontact korganizer ksystemlog ktimer kwalletmanager okular partitionmanager pim-data-exporter pim-sieve-editor signon-kwallet-extension spectacle zeroconf-ioslave gst-libav gst-plugins-good gst-plugins-bad gst-plugins-ugly kcm-wacomtablet colord-kde firefox ttf-dejavu ttf-liberation ttf-croscore ttf-fira-mono ttf-fira-sans noto-fonts noto-fonts-emoji noto-fonts-extra ttf-roboto steam-native-runtime discord telegram-desktop signal-desktop libreoffice gimp krita qbittorrent android-tools neofetch celluloid unrar p7zip zip lzop lrzip sshfs packagekit-qt5 qt5-tools torbrowser-launcher fwupd fwupd-efi pam-u2f libfido2 nextcloud-client firefox-ublock-origin firefox-stylus lutris retroarch lmms obs-studio abcde ffmpeg flac strawberry snapper gnome-disk-utility bluez-utils adobe-source-han-sans-otc-fonts adobe-source-han-sans-jp-fonts adobe-source-han-serif-otc-fonts adobe-source-han-serif-jp-fonts ttf-hannom ttf-khmer ttf-tibetan-machine
#For NVIDIA
sudo pacman -S nvidia-dkms nvidia-settings libva-vdpau-driver vdpauinfo
#For Intel
#sudo pacman -S xf86-video-intel intel-media-driver intel-gpu-tools libva-utils vulkan-intel
#For older Intel
#sudo pacman -S xf86-video-intel libva-intel-driver intel-gpu-tools libva-utils vulkan-intel
#Out-Of-Repo Package Installs
yay -S ttf-crosextra proton-ge-custom-bin protonmail-bridge-bin protonvpn snap-pac-grub zramswap
#Group addition (change user name as needed)
sudo usermod -aG lp waffle
sudo usermod -aG adbusers waffle
#Set up some configuration
#Need to set up localhost resolution, add apparmor boot options, and configure snapper yourself
sudo su -c 'wget -O /etc/samba/smb.conf https://raw.githubusercontent.com/zentyal/samba/master/examples/smb.conf.default'
sudo su -c 'echo "export PLASMA_USE_QT_SCALING=1" >> /etc/profile'
sudo su -c 'echo "export GTK_USE_PORTAL=1" >> /etc/profile'
#sudo su -c 'echo "export MOZ_X11_EGL=1" >> /etc/profile'
#SystemD services
sudo systemctl enable cups
sudo systemctl enable cups-browsed
sudo systemctl enable avahi-daemon
sudo systemctl enable smb
sudo systemctl enable nmb
sudo systemctl enable apparmor
sudo systemctl enable sddm
sudo systemctl enable zramswap
sudo systemctl enable bluetooth
exit 0

